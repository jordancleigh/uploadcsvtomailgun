﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace UploadCsvToMailgun
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // CREATE CSV
            string data = "address,domain\njordan.cleigh@fmgsuite.com,\njordan.cleigh2@fmgsuite.com,\nfoo@example.com,\nbar@example.org,\nnew@example.net,\naddress@example.com,"; // Replace with SQL data
            string filePath = @"File.csv";
            File.WriteAllText(filePath, data);


            // XMIT CSV
            string apiUrl = "https://api.mailgun.net/v3/email01.dev.fmgsuite.com/whitelists/import";

            HttpClient httpClient = new HttpClient();
            string auth = Convert.ToBase64String(Encoding.UTF8.GetBytes("api:REPLACE-ME-WITH-YOUR-API-KEY")); // REPLACE RIGHT SIDE OF STRING WITH ACTUAL API KEY
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Basic {auth}");

            // No caching
            CacheControlHeaderValue cacheControl = new CacheControlHeaderValue();
            cacheControl.NoCache = true;
            httpClient.DefaultRequestHeaders.CacheControl = cacheControl;

            byte[] bytes = File.ReadAllBytes(filePath);
            HttpContent fileContent = new ByteArrayContent(bytes);
            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("text/csv");

            try
            {
                DateTime start = DateTime.Now;
                var response = await httpClient.PostAsync(apiUrl, new MultipartFormDataContent
                {
                    {fileContent, "\"file\"", "\"File.csv\""}
                });
                DateTime end = DateTime.Now;
                TimeSpan diff = end.Subtract(start);

                Console.WriteLine(response);
                Console.WriteLine($"HTTP request took {diff.TotalSeconds} seconds");
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                Console.WriteLine(message);
            }            
        }
    }
}
